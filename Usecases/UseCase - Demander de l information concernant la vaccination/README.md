#Cas d'utilisation
##CU: Prise de rendez-vous
##But: Prise de rendez-vous pour une personne qui désire se faire vacciner au centre appartenant à GoodPeople
##Acteurs: Visiteur planifié (principal), Employé (secondaire)
##Préconditions: Le visiteur planifié doit appeler l’employé chez GoodPeople
#Scénario principal

1. Le client appelle l’employé chez GoodPeople pour une prise de rendez-vous.
2. L’employé demande au client son nom, prénom, une date et heure potentielle pour un rendez-vous ainsi que le type de vaccin qu’il veut recevoir. 
3. L’employé demande si le client prend aussi un rendez-vous pour une autre personne qui veut se faire vacciner.
4. Le client indique que non.
5. L’employé ouvre le terminal VaxTodo.
6. Le terminal demande à l’employé d’entrer ses informations d’authentification.
7. L’employé entre son code d’employé à 9 chiffres ainsi que son mot de passe à 8 caractères.
8. Le système valide l’authentification de l’employé.
9. Le système affiche le calendrier des rendez-vous.
10. L’employé entre dans le terminal la date et la plage horaire demandé par le client.
11. Le système valide que la date et la plage horaire d'entrées est disponible.
12. Le système demande si l’employé veut prendre un rendez-vous à cette date et heure.
13. L’employé confirme.
14. Le terminal demande le nom et prénom du client ainsi que le type de vaccin qu’il veut parmi ceux disponibles.
15. L’employé entre les informations du client dans le terminal.
16. L’employé confirme auprès du client que ses informations personnelles, la plage horaire, la date du rendez-vous ainsi que son type de dose sont valides.
17. Le client confirme toutes les informations.
18. L’employé soumet le formulaire au terminal.
19. Le système valide les informations.
20. Le terminal confirme que la prise de rendez-vous à été réussie. 
21. Le terminal affiche le numéro de réservation unique du rendez-vous, le prénom et nom du visiteur, la date et l’heure de la visite ainsi que le type de dose.
22. L’employé confirme au client que la prise de rendez-vous a été réussie et communique les détails du rendez-vous au client.
23. Le client confirme que toutes les informations sont bonnes**(redondant)
24. Le système envoie par courriel au client tous les détails de son rendez-vous ainsi que son numéro de réservation unique.
25. L’employé demande au client si celui-ci a d’autres questions concernant la prise de vaccination.
26. Le client dit qu’il n’a pas d’autres questions.
27. Le client raccroche son téléphone.

##Scénarios alternatifs
1a. Le client pose des questions au sujet des pièces requises et/ou des heures d’ouvertures
1a.1. L’employé informe le client que le centre de vaccination est ouvert du lundi au vendredi, sauf les jours fériés, de 8h00 à 18h00.
1a.2. L’employé informe le client qu’il doit amener sa carte d’assurance maladie ainsi que son numéro de réservation unique lors de son rendez-vous.
1a.3. L’employé informe le client que si celui-ci vient au centre uniquement pour sa deuxième dose, alors il doit avoir sa preuve de vaccination pour sa première dose.
1a.4. Le scénario reprend à l’étape 2.

4a. Le client souhaite aussi prendre rendez-vous pour une autre personne
4a.1. L’employé demande les informations personnelles de cette deuxième personne soit son nom, prénom, une date et heure potentielle pour un rendez-vous ainsi que le type de vaccin qu’elle veut recevoir
4a.2. Le scénario reprend à l’étape 5.

6a. L’employé n’arrive pas à s’authentifier (mauvaise saisie des informations du compte).
  6a.1. Le système n’a pas trouvé le compte de l’employé et/ou son mot de passe.
  6a.2. Le système affiche que son numéro de compte et/ou son mot de passe ne sont pas    valide.
  6a.3. Le scénario reprend à l’étape 6.
       6a.1.1. L’employé appelle le support technique pour VaxTodo. 
       6a.1.2. Le support technique de VaxTodo règle les problèmes    techniques. 
       6a.1.3. L’employé rentre de nouveau ses informations d’authentification dans le terminal VaxTodo.
       6a.1.2. Le système authentifie l’employé. 
       6a.1.3. Le scénario reprend à l’étape 7.
À enlever???

11a. La date du rendez-vous est non valide.
11a.1. Le système trouve la date dans le calendrier, mais celle-ci est un jour férié.
11a.2. Le système affiche Date non valide à l’écran.
11a.3. L’employé informe le client que la date du rendez-vous est invalide.
11a.4. L’employé demande une autre date pour prendre le rendez-vous.
11a.5. Le client choisit une nouvelle date pour le rendez-vous et en informe l’employé.
11a.6. Le scénario reprend à l’étape 10.

11b. L’heure du rendez-vous est non valide.
11b.1. Le système trouve la date dans le calendrier ainsi que l’heure, mais celle-ci est hors des heures d’ouvertures du centre de vaccination.
11b.2. Le système affiche Plage horaire non valide à l’écran.
11b.3. L’employé informe le client que la plage horaire du rendez-vous est invalide.
11b.4. L’employé informe le client des heures d’ouvertures du centre et lui demande une plage horaire valide.
11b.5. Le client choisit une nouvelle plage horaire pour le rendez-vous et en informe l’employé.
11b.6. Le scénario reprend à l’étape 10.

11c. La date et/ou l’heure du rendez-vous est invalide. (N’est pas 72 heures à l’avance)
11c.1. Le système trouve la date et l’heure dans le calendrier, mais le rendez-vous est dans moins de 72 heures.
11c.2. Le système affiche Date et/ou heure non valide. Moins de 72 heures avant le rendez-vous à l’écran.
11c.3. L’employé informe le client qu’il doit avoir pris le rendez-vous au minimum 72 heures à l’avance.
11c.4. L’employé demande au client une autre date et/ou une plage horaire pour le rendez-vous.
11c.5. Le client choisit une autre date et/ou plage horaire et en informe l’employé.
11c.6. Le scénario reprend à l’étape 10.


11d. La capacité maximale du centre de vaccination est atteinte.
11d.1. Le système vérifie le nombre de rendez-vous dans la plage horaire indiqué.
11d.2. Le système affiche Capacité maximale atteinte à l’écran.
11d.3.L’employé informe le client qu’il n’y a plus de place disponible dans cette plage horaire.
11d.4. L’employé demande au client une autre plage horaire pour son rendez-vous.
11d.5. Le client choisit une autre plage horaire et en informe l’employé.
11d.6. Le scénario reprend  à l’étape 10.

11e. Le délai d’un mois entre sa première et deuxième dose n’est pas respecté.
11e.1. Le système vérifie que le nombre de jours entre la première vaccination du client et la date sélectionnée pour son rendez-vous de deuxième dose est de 1 mois.
11e.2. Le système affiche Délai non respecté à l’écran.
11e.3. L’employé informe le client qu’il doit y avoir au minimum un mois entre ses deux vaccins et donc que la date de son rendez-vous est invalide.
11e.4. L’employé demande au client de choisir une nouvelle date pour son deuxième rendez-vous.
11e.5. Le client choisit une nouvelle date et en informe l’employé.
11e.6. Le scénario reprend à l’étape 10.
19a. Le système rejette certaines informations entrées par l’employé.
19a.1. Le système demande à l’employé d’entrer de nouveau les informations.
19a.2. Le scénario reprend à l’étape 15.
