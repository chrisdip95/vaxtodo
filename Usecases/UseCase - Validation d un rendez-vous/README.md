# CU: Validation de l’existence d’un rendez-vous
## But: Un employé vérifie si la le visiteur a un rendez-vous
## Acteurs: Employé (principal)
## Préconditions: L’employé est authentifié dans le système VaxTodo et a un numéro de réservation
## Scénario principal
L’employé demande au client son numéro de réservation pour son rendez-vous.
Le client lui communique l’information.
L’employé ouvre le terminal VaxTodo.
Le terminal demande à l’employé d’entrer ses informations d’authentification.
L’employé entre son code d’employé à 9 chiffres et son mot de passe à 8 caractères.
Le système valide l’authentification de l’employé.
Le système affiche le calendrier des rendez-vous.
L’employé entre le numéro de réservation du client dans le terminal.
Le terminal affiche les informations concernant le rendez-vous du client et ainsi valide celui-ci.

5. L’employé ouvre le terminal VaxTodo.
6. Le terminal demande à l’employé d’entrer ses informations d’authentification.
7. L’employé entre son code d’employé à 9 chiffres ainsi que son mot de passe à 8 caractères.
8. Le système valide l’authentification de l’employé.
9. Le système affiche le calendrier des rendez-vous.

L’employé accède à l’option calendrier
L’employé sélectionne vérifier date et heure de rendez-vous
L’employé entre la date et l’heure du rendez-vous à valider
Le système affiche que la date et l’heure du rendez-vous existe
## Scénarios alternatifs

