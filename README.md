# VaxTodo IFT2255

Pour prêter main-forte à la campagne de vaccination, en coordination avec le gouvernement la compagnie GoodPeople a entrepris l'initiative VaxTodo.
Cette initiative permet à quiconque de prendre un rendez-vous avec GoodPeople pour se faire vacciner. La compagnie assure aussi le suivi pour les futures doses et autres requêtes liées à la vaccination.

L'initiative a été très bien reçue, dépassant toutes leurs attentes. Cependant, aujourd'hui ils ont de plus en plus de misère à respecter les dates fixées avec les clients et faire le suivi dans un délai raisonnable.
Jusqu'à présent, n'importe qui peut se présenter au comptoir et procéder à la vaccination ou prendre un rendez-vous par appel téléphonique. Afin d'assurer le service à tous dans des délais raisonnables et sans accroc, GoodPeople fait appel à vous pour monter un système qui permet de garantir les périodes réservées et faciliter le suivi.

Vous avez droit à 3 rencontres avec le client (communication via Discord ou courriel) pour demander des informations additionnelles et comprendre leur besoin.
Les réponses aux questions seront ajoutées au corps du texte plus haut.
Jusqu'au 30 septembre, des précisions peuvent être rajoutées au texte.

GoodPeople attend vos questions.

# Fait par 
- Blanchette Lilou 
- Clément Max-antoine
- Dip Christopher
