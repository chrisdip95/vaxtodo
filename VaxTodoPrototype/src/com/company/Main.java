package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // Inspired by: https://stackoverflow.com/questions/45117385/want-to-return-to-main-menu-in-switch-case

        boolean exit = false;
        do {
            System.out.println("VaxTodo Menu");
            System.out.println("1. Gestion de la prise de rendez-vous");
            System.out.println("2. Gestion des employés et bénévoles");
            System.out.println("3. Gestion des comptes visiteurs");
            System.out.println("4. Traitement de visiteur");
            System.out.println("5. Envoyer un suivi");
            System.out.println("6. Exit");
            Scanner sc1 = new Scanner(System.in);
            String choice = sc1.nextLine();
            switch (choice) {
                case "1":
                    System.out.println("VaxTodo Menu: Gestion de la prise de rendez-vous");
                    System.out.println("1. Prendre un rendez-vous");
                    System.out.println("2. Vérifier un rendez-vous");
                    System.out.println("3. Changer un rendez-vous");
                    System.out.println("4. Annuler un rendez-vous");
                    System.out.println("5. Retour au menu principal");
                    Scanner sc2 = new Scanner(System.in);
                    String subMenuChoice = sc2.nextLine();
                    switch (subMenuChoice){
                        case "1":priseDeRendezVous();break;
                        case "2":validationAUnRendezVous();break;
                        case "3":changementDeRendezVous();break;
                        case "4":annulationDeRendezVous();break;
                        case "5":break;
                    }

                case "2":
                    System.out.println("VaxTodo Menu: Gestion des employés et bénévoles");
                    System.out.println("1. Afficher les employés et bénévoles");
                    System.out.println("2. Ajouter un employé ou bénévole");
                    System.out.println("3. Modifier un employé ou bénévole");
                    System.out.println("4. Enlever un employé ou bénévole");
                    System.out.println("5. Retour au menu principal");
                    Scanner sc3 = new Scanner(System.in);
                    String subMenuChoiceTwo = sc3.nextLine();
                    break;
                case "3":
                    System.out.println("VaxTodo Menu: Gestion des comptes visiteurs");
                    System.out.println("1. Afficher les comptes visiteurs");
                    System.out.println("2. Ajouter un compte visiteur");
                    System.out.println("3. Modifier un compte visiteur");
                    System.out.println("4. Enlever un compte visiteur");
                    System.out.println("5. Retour au menu principal");
                    Scanner sc4 = new Scanner(System.in);
                    String subMenuChoiceThree = sc4.nextLine();
                    break;

                case "4":
                    System.out.println("VaxTodo Menu: Traitement de visiteur");
                    Scanner sc5 = new Scanner(System.in);

                    System.out.println("Visiteur planifié? O/N");
                    String subMenuLastAnswer = sc5.nextLine();

                    if(subMenuLastAnswer.equals("O")) validationAUnRendezVous();
                    else validationAvecHeureDeRendezVous();

                    System.out.println("Est-ce qu'il s'agit de la première visite? O/N");
                    subMenuLastAnswer = sc5.nextLine();

                    if(subMenuLastAnswer.equals("O")){
                        creationDeCompte();
                        System.out.println("Est-ce que le visiteur veut planifier la prochaine visite? O/N");
                        if(subMenuLastAnswer.equals("O")) priseDeRendezVous(); // Donc ceci veut dire que traitement de visiteur INCLUDES une prise de rendez vous use case?? (IMPORTANT)
                    }
                    else validerIdentiteAvecCompte();

                    remplirFormulaire();

                    break;
                case "5":
                    envoyerSuivi();
                case "6":
                    exit = true;
                    break;
            }
        } while (!exit);
    }
    public static void priseDeRendezVous(){
        System.out.println("Prise de rendez-vous");
    }
    public static void validationAUnRendezVous(){
        System.out.println("Validation a un rendez-vous");

    }
    public static void changementDeRendezVous(){
        System.out.println("Changement de rendez-vous");

    }
    public static void annulationDeRendezVous(){
        System.out.println("Annulation de rendez-vous rendez-vous");
    }
    public static void creationDeCompte(){
        System.out.println("Création de compte");

    }
    public static void envoyerSuivi(){
        System.out.println("Envoyer un suivi");
    }
    public static void demandeDePreuveDeVaccination(){
        envoyerSuivi();
    }

    public static void remplirFormulaire(){
        Scanner sc1 = new Scanner(System.in);
        String subMenuLastAnswer;
        System.out.println("Entrez le Nom");
        subMenuLastAnswer = sc1.nextLine();
        System.out.println("Entrez le Prénom");
        subMenuLastAnswer = sc1.nextLine();
        System.out.println("Entrez le Date de naissance");
        subMenuLastAnswer = sc1.nextLine();
        System.out.println("Date de la visite");
        subMenuLastAnswer = sc1.nextLine();
        System.out.println("Avez-vous déjà reçu une première dose? O/N");
        subMenuLastAnswer = sc1.nextLine();
        System.out.println("Avez-vous déjà contracté la COVID? O/N");
        subMenuLastAnswer = sc1.nextLine();
        System.out.println("Avez-vous des symptômes de la COVID? O/N");
        subMenuLastAnswer = sc1.nextLine();
        System.out.println("Avez-vous des allergies? O/N");
        subMenuLastAnswer = sc1.nextLine();
        System.out.println("Quel type de vaccin souhaitez-vous recevoir?");
        System.out.println("1. Pfizer");
        System.out.println("2. Moderna");
        System.out.println("3. Astrazeneca");
        System.out.println("4. Johnson & Johnson");
        subMenuLastAnswer = sc1.nextLine();

    }

    public static void validerIdentiteAvecCompte(){
        System.out.println("Valider idendité avec un compte");
    }

    public static void validationAvecHeureDeRendezVous(){
        System.out.println("Validation de l'heure de rendez-vous pour un visiteur spontané");
    }
}
